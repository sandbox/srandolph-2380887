backup_migrate_googledrive
==========================
Drupal module to work with backup_migrate and direct files to a Google Drive

https://console.developers.google.com/project
Sign in with a Google account
API Project
 In Left Column -> APIs & auth
                -> Credentials
Create new Client ID
  Web application, Configure consent screen
    Required: email address & Producte name
Create Client ID after inserting web site name

SandBox Stuff
mkdir backup_migrate_google_drive
cd backup_migrate_google_drive
git init
git checkout -b 7.x-1.x
echo "name = Backup Migrate Google Drive" > backup_migrate_google_drive.info
git add backup_migrate_google_drive.info
git commit -m "Initial commit."
git remote add origin srandolph@git.drupal.org:sandbox/srandolph/2380887.git
git push origin 7.x-1.x
